import numpy as np
import torch

class GaussianNoise:
    def __init__(self, noise_params=None):
        """
        Initializes the Gaussian noise generator with customizable parameters.

        Parameters
        ----------
        noise_params : dict, optional
            A dictionary where keys are input names (e.g., "tracks", "jets") and values are 
            dictionaries with "mean", "std", and optionally a "mask" array for selective noise.
            Example:
            {
                "tracks": {"mean": 0.0, "std": 0.1, "mask": [1, 0, 1, 0, 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0]}, # Apply noise to field 0 and 2
                "jets": {"mean": 0.0, "std": 0.05, "mask": [1, 0]}      # Apply noise only to field 0
            }
        """
        self.noise_params = noise_params if noise_params is not None else {}

    def add_noise(self, data, dataset_name):
        """
        Add Gaussian noise to specified subfields of a dataset.
        """
        #print(f"[DEBUG] Available noise configurations: {list(self.noise_params.keys())}")
        #print(f"[DEBUG] Processing dataset: {dataset_name}")

        #if isinstance(data, np.ndarray):
        #    print(f"[DEBUG] Data shape: {data.shape}")
        #else:
        #    print("[DEBUG] Data is not an ndarray.")
            
        # Check if noise configuration exists for this dataset
        #if dataset_name in self.noise_params:
        #    print(f"[DEBUG] Adding noise to {dataset_name}")
        #if "jets" not in self.noise_params:
        #    print("[ERROR] 'jets' not found in noise configuration. Please check the setup.")
        #else:
        #    print(f"[DEBUG] Noise configuration for 'jets': {self.noise_params['jets']}")

        if dataset_name not in self.noise_params:
            #print(f"[DEBUG] No noise config found for dataset '{dataset_name}'. Skipping.")
            return data
    
        noise_config = self.noise_params[dataset_name]
    
        # Extract parameters
        mean = noise_config.get("mean", 0.0)
        std = noise_config.get("std", 0.0)
        mask = noise_config.get("mask", None)
    
        #print(f"[DEBUG] Noise parameters - mean: {mean}, std: {std}, mask: {mask}")
        #print(f"[DEBUG] Original data shape: {data.shape}")
    
        # Generate noise
        noise = np.random.normal(mean, std, data.shape)
        #print(f"[DEBUG] Generated noise sample: {noise.flatten()[:5]}")
    
        if mask is not None:
            # Convert mask to a broadcastable format
            mask = np.array(mask)
            while len(mask.shape) < len(data.shape):
                mask = np.expand_dims(mask, axis=0)  # Aggiungi dimensioni in cima
            #print(f"[DEBUG] Applying mask with shape: {mask.shape}")
            noise *= mask
    
        # Apply noise to the data
        data += noise
        #print(f"[DEBUG] Data after noise shape: {data.shape}")
    
        return data
